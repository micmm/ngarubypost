class AddNewColumnToMyTable < ActiveRecord::Migration
  def change
  	add_column :articles, :published, :DateTime
  end
end
