require 'rails_helper'

RSpec.describe ArticlesController, :type => :controller do
	describe "GET #index" do
		it "rellena un array de articulos" do
			article = FactoryGirl.create(:article)
			get :index
			assigns(:articles).should eq([article])
		end
		it "renderiza la vista :index" do
			get :index
			response.should render_template :index
		end
	end

	describe "GET #show" do
		it "asigna el articulo solicitado a @article" do
			article = FactoryGirl.create(:article)
			get :show, id: article
			assigns(:article).should eq(article)
		end
		it "renderiza la vista :show" do
			get :show, id: FactoryGirl.create(:article)
			response.should render_template :show
		end
	end

	describe "GET #new" do
		it "asigna un nuevo artículo a @article" do
			get :new
			expect(assigns(:article)).to be_a_new(Article)
		end

		it "renderiza la vista :new" do
			get :new
			response.should render_template :new
		end
	end

	describe "POST #create" do
		context "con parámetros válidos" do
			it "salva el nuevo artículo en la base de datos" do
				expect{
					post :create, article: FactoryGirl.attributes_for(:article)
				}.to change(Article,:count).by(1)
			end
			it "redireciona a la página de inicio" do
				post :create, article: FactoryGirl.attributes_for(:article)
				response.should redirect_to Article.last
			end
		end

		context "con parámetos no válidos" do
			it "no guarda el artículo en la base de datos" do
				expect{
					post :create, article: FactoryGirl.attributes_for(:invalid_article)	
				}.to_not change(Article,:count)
				
			end
			it "vuelve a mostrar la vista :new" do
				post :create, article: FactoryGirl.attributes_for(:invalid_article)
				response.should render_template :new
			end
		end
	end

end
