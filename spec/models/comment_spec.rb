require 'rails_helper'

RSpec.describe Comment, :type => :model do
  	it "tiene un factory valido" do
		FactoryGirl.create(:comment).should be_valid
	end
	it "es invalido sin nombre" do
		FactoryGirl.build(:comment, commenter: nil).should_not be_valid
	end
	it "es invalido sin comentario" do
		FactoryGirl.build(:comment, body: nil).should_not be_valid
	end
end
