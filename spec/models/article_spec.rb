require 'rails_helper'

RSpec.describe Article, :type => :model do
	#fixtures :articles
	it "tiene un factory valido" do
		FactoryGirl.create(:article).should be_valid
	end
	it "es invalido sin titulo" do
		FactoryGirl.build(:article, title: nil).should_not be_valid
	end
	it "es valido sin texto" do
		FactoryGirl.build(:article, text: nil).should be_valid
	end
	it "es valido sin fecha" do
		FactoryGirl.build(:article, published: nil).should be_valid
	end
	
end
