require 'faker'

FactoryGirl.define do
	factory :article do |f|
		f.title {Faker::Lorem.sentence }
		f.text {Faker::Lorem.paragraph}
		f.published {Date.today - Faker::Number.number(3).to_i.days}
	end

	factory :invalid_article, parent: :article do |f|
		f.title nil
	end
end