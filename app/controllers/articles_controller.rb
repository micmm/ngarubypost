class ArticlesController < ApplicationController


	#http_basic_authenticate_with name: "dhh", password: "secret", except: [:index, :show]
 

	def new
		@article = Article.new
	end

	def create
  		# render plain: params[:article].inspect     -> Inspeccion de parámetros pasados por el formulario
  		# @article = Article.new(params[:article])   -> No se pueden acceder directamente a los parámetros
  		

  		@article = Article.new(article_params.merge :published => Date.current())
 
  		if @article.save
		    redirect_to @article
		else
		    render 'new'
		end
	end

	def show
  		@article = Article.find(params[:id])
	end

	def index
		@articles = Article.all
	end

	def edit
	  @article = Article.find(params[:id])
	end

	def update
	  @article = Article.find(params[:id])
	 
	  if @article.update(article_params.merge :published => Date.current())
	    redirect_to @article
	  else
	    render 'edit'
	  end
	end

	def destroy
	  @article = Article.find(params[:id])
	  @article.destroy
	 
	  redirect_to articles_path
	end

	private
		def article_params
	   		params.require(:article).permit(:title, :text)
	  	end
end
